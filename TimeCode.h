#ifndef MOVIEDATABASE_TIMECODE_H
#define MOVIEDATABASE_TIMECODE_H
#include "Movie.h"
#include <sstream>
#include <stdexcept>
#include <iomanip>
#include <vector>
#include <iostream>

using namespace std;

class TimeCode {

private:

    int day, month, year, hour, minute, second;

public:

    TimeCode(int newDay, int newMonth, int newYear, int newHour, int newMinute, int newSecond);

    TimeCode();

    ~TimeCode();

    void setDay(int newDay);

    void setMonth(int newMonth);

    void setYear(int newYear);

    void setHour(int newHour);

    void setMinute(int newMinute);

    void setSecond(int newSecond);

    inline int getDay() const{
        return this->day;
    }

    inline int getMonth() const{
        return this->month;
    }

    inline int getYear() const{
        return this->year;
    }

    inline int getHour() const{
        return this->hour;
    }

    inline int getMinute() const{
        return this->minute;
    }

    inline int getSecond() const{
        return this->second;
    }

    operator int() const;

    bool operator==(TimeCode& tc);

    bool operator!=(TimeCode& tc);

    bool operator>(TimeCode& tc);

    bool operator<(TimeCode& tc);

    bool operator>=(TimeCode& tc);

    bool operator<=(TimeCode& tc);

    static vector<string> split(string line, char delimiter);

};

inline ostream& operator<<(ostream& str, const TimeCode* tc){
    //date
    str << std::setw(2) << std::setfill('0') << tc->getDay() << "/";
    str << std::setw(2) << std::setfill('0') << tc->getMonth() << "/";
    str << std::setw(4) << std::setfill('0') << tc->getYear() << "T";

    //time
    str << std::setw(2) << std::setfill('0') << tc->getHour() << ":";
    str << std::setw(2) << std::setfill('0') << tc->getMinute() << ":";
    str << std::setw(2) << std::setfill('0') << tc->getSecond() << "Z";

    return str;
}

inline istream& operator>>(istream& str, TimeCode* tc){
    //fields that are needed for TimeCode
    int day, month, year, hour, minute, second;

    string time, date;

    string tempString;
    getline(str,tempString);
    vector<string> tempVector = TimeCode::split(tempString, 'T');

    date = tempVector.at(0);
    time = tempVector.at(1);

    //have to get rid of Z at the end
    date = date.substr(0, date.length());

    //get rid of '/' in date
    tempVector = TimeCode::split(date, '/');
    day = stoi(tempVector.at(0));
    month = stoi(tempVector.at(1));
    year = stoi(tempVector.at(2));

    //get rid of ':' in time
    tempVector = TimeCode::split(time, ':');
    hour = stoi(tempVector.at(0));
    minute = stoi(tempVector.at(1));
    second = stoi(tempVector.at(2));

    try{
        tc->setYear(year);
        tc->setDay(day);
        tc->setHour(hour);
        tc->setMinute(minute);
        tc->setMonth(month);
        tc->setSecond(second);
    }catch(const invalid_argument& e){
        cerr << "Exception: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    return str;
}

//idea is taken from https://autohotkey.com/board/topic/2486-code-to-convert-fromto-unix-timestamp/
inline TimeCode::operator int() const{
    //get year seconds
    int year_sec = 31536000 * (year - 1970);

    //Determine how many leap days
    int leap_days = (year - 1972)/4 + 1;

    //Determine if date is in a leap year, and if the leap day has been yet
    if(year % 4 == 0 && month <= 2){
        //leap day has not been yet
        leap_days -= 1;
    }

    int leap_sec = leap_days*86400;

    //Determine fully completed months
    int month_sec;
    if (month == 1){
        month_sec = 0;
    } else if (month == 2){
        month_sec = 2678400;
    } else if (month == 3){
        month_sec = 5097600;
    } else if (month == 4){
        month_sec = 7776000;
    } else if (month == 5){
        month_sec = 10368000;
    } else if (month == 6){
        month_sec = 13046400;
    } else if (month == 7){
        month_sec = 15638400;
    } else if (month == 8){
        month_sec = 18316800;
    } else if (month == 9){
        month_sec = 20995200;
    } else if (month == 10){
        month_sec = 23587200;
    } else if (month == 11){
        month_sec = 26265600;
    } else if (month == 12){
        month_sec = 28857600;
    }

    //Determine fully completed days
    int day_sec = (day - 1) * 86400;

    //Determine fully completed hours
    int hour_sec = hour * 3600;

    //Determine fully completed minutes
    int min_sec = minute * 60;

    //Calculate total seconds
    return year_sec + month_sec + day_sec + leap_sec + hour_sec + min_sec + second;
}

inline bool TimeCode::operator==(TimeCode& tc){
    //could cast to int but this works faster than overloaded int() operator I believe
    return this->day == tc.day && this->month == tc.month &&
            this->year == tc.year && this->hour == tc.hour &&
            this->minute == tc.minute && this->second == tc.second;
}

inline bool TimeCode::operator!=(TimeCode& tc){
    return !this->operator==(tc);
}

inline bool TimeCode::operator>(TimeCode& tc){
    //could cast to int but this works faster than overloaded int() operator I believe
    //even though it's ugly :(
    if(this->year > tc.year){
        return true;
    }else if(this->year == tc.year){
        if(this->month > tc.month){
            return true;
        }else if(this->month == tc.month){
            if(this->day > tc.day){
                return true;
            }else if(this->day == tc.day){
                if(this->hour > tc.hour){
                    return true;
                }else if(this->hour == tc.hour){
                    if(this->minute > tc.minute){
                        return true;
                    }else if(this->minute == tc.minute){
                        if(this->second > tc.second){
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;
}

inline bool TimeCode::operator<=(TimeCode& tc){
    return !this->operator>(tc);
}

inline bool TimeCode::operator<(TimeCode& tc){
    //could cast to int but this works faster than overloaded int() operator I believe
    //even though it's ugly :(
    if(this->year < tc.year){
        return true;
    }else if(this->year == tc.year){
        if(this->month < tc.month){
            return true;
        }else if(this->month == tc.month){
            if(this->day < tc.day){
                return true;
            }else if(this->day == tc.day){
                if(this->hour < tc.hour){
                    return true;
                }else if(this->hour == tc.hour){
                    if(this->minute < tc.minute){
                        return true;
                    }else if(this->minute == tc.minute){
                        if(this->second < tc.second){
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;
}

inline bool TimeCode::operator>=(TimeCode& tc){
    return !this->operator<(tc);
}
#endif
