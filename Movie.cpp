#include <algorithm>
#include <sstream>
#include "Movie.h"

Movie::Movie(){
    this->ratings = 0;
    this->meanRating = 0;
}

Movie::~Movie() {
    //not sure if this line should be here, but just in case..
    delete this;
}

Movie::Movie(string newTitle, int newYear, string newMPAArating, string newGenres, int newLength,
             double newMeanRating, int newRatings){

    //check if the passed title is empty
    if(newTitle.empty()){
        throw invalid_argument("The passed title is NULL or an empty string.");
    }

    //get current year
    time_t theTime = time(NULL);
    auto aTime = localtime(&theTime);
    int currentYear = aTime->tm_year + 1900;

    //check if the passed year is less or equal to 0 or more than the current year
    if(newYear <= 0 || newYear > currentYear){
        throw invalid_argument("The passed year is less or equal to 0 or more than the current year.");
    }

    //check if the passed MPAArating is empty
    if(newMPAArating.empty()){
        throw invalid_argument("The passed MPAArating is NULL or an empty string.");
    }

    //check if the passed genre(s) is empty
    if(newGenres.empty()){
        throw invalid_argument("The passed genre is NULL or an empty string.");
    }

    //check if the passed length is less or equal to 0
    if(newLength <= 0){
        throw invalid_argument("The passed length is less or equal to 0.");
    }

    this->title = newTitle;
    this->year = newYear;

    istringstream tempStream(newMPAArating);
    tempStream >> this->MPAArating;

    this->genre = split(newGenres, '/');
    this->length = newLength;
    this->meanRating = 0;
    this->ratings = 0;
}

void Movie::setTitle(string newTitle) {
    //check if the passed title is empty
    if(newTitle.empty()){
        throw invalid_argument("The passed title is NULL or an empty string.");
    }

    this->title = newTitle;
}

void Movie::setYear(int newYear){
    //get current year
    time_t theTime = time(NULL);
    auto aTime = localtime(&theTime);
    int currentYear = aTime->tm_year + 1900;

    //check if the passed year is less or equal to 0 or more than the current year
    if(newYear <= 0 || newYear > currentYear){
        throw invalid_argument("The passed year is less or equal to 0 or more than the current year.");
    }

    this->year = newYear;
}

void Movie::setMPAArating(string newMPAArating) {
    //check if the passed MPAArating is empty
    if(newMPAArating.empty()){
        throw invalid_argument("The passed MPAArating is NULL or an empty string.");
    }

    istringstream tempStream(newMPAArating);
    tempStream >> this->MPAArating;
}

void Movie::setGenre(string newGenres) {
    //check if the passed genre(s) is empty
    if(newGenres.empty()){
        throw invalid_argument("The passed genre is NULL or an empty string.");
    }

    this->genre = split(newGenres, '/');
}

void Movie::setLength(int newLength) {
    //check if the passed length is less or equal to 0
    if(newLength <= 0){
        throw invalid_argument("The passed length is less or equal to 0.");
    }

    this->length = newLength;
}

void Movie::setRatings(int newRatings){
    if(newRatings < 0){
        throw invalid_argument("The amount of ratings cannot be negative.");
    }

    this->ratings = newRatings;
}

void Movie::setMeanRating(double newMeanRating){
    if(newMeanRating <= 0 || newMeanRating > 11){
        throw invalid_argument("The passed rating is not in range 1-11.");
    }

    this->meanRating = newMeanRating;
}

vector<string> Movie::split(string line, char delimiter) {
    vector<string> vector;

    //turn the string into a stream
    stringstream ss(line);

    string tempString;

    //split the line (stream) by the passed delimiter and add one by one to the vector
    while(getline(ss, tempString, delimiter)) {
        vector.push_back(tempString);
    }

    return vector;
}

//I wanted to make it "rating >> movie" operator
//but could not manage to chain header files properly..
void Movie::addRating(int rating){
    double sum = this->getMeanRating() * this->getRatings();
    this->setMeanRating((sum + rating) / (this->getRatings() + 1));
    this->setRatings(this->getRatings() + 1);
}