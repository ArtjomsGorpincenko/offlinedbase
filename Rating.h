#ifndef MOVIEDATABASE_RATING_H
#define MOVIEDATABASE_RATING_H

#include "TimeCode.h"
#include <stdexcept>
#include <iomanip>
#include <vector>

using namespace std;

class Rating {

private:
    TimeCode* timeCode;
    string author;
    string movieTitle;
    int rating;

public:
    Rating(TimeCode* newTimeCode, string newAuthor, string newMovieTitle, int newRating);

    Rating();

    ~Rating();

    inline TimeCode* getTimeCode() const{
        return this->timeCode;
    }

    inline string getAuthor() const{
        return this->author;
    }

    inline string getMovieTitle() const{
        return this->movieTitle;
    }

    inline int getRating() const{
        return this->rating;
    }

    void setTimeCode(TimeCode* newTimeCode);

    void setAuthor(string newAuthor);

    void setMovieTitle(string newMovieTitle);

    void setRating(int newRating);

    bool operator==(Rating& mov);

    bool operator!=(Rating& mov);

    bool operator>(Rating& mov);

    bool operator<(Rating& mov);

    bool operator>=(Rating& mov);

    bool operator<=(Rating& mov);
};

inline ostream& operator<<(ostream& str, const Rating* rt){
    str << rt->getTimeCode() << ",\"" << rt->getAuthor() << "\",\""
        << rt->getMovieTitle() << "\"," << rt->getRating() << endl;

    return str;
}

inline istream& operator>>(istream& str, Rating* rt){
    //fields that are needed for Rating
    int rating;

    string timeCode, author, movieTitle;

    string tempString;
    getline(str,tempString);
    vector<string> tempVector = Movie::split(tempString, '\"');

    timeCode = tempVector.at(0);
    timeCode = timeCode.substr(0, timeCode.length() - 1);

    author = tempVector.at(1);
    movieTitle = tempVector.at(3);

    rating = stoi(tempVector.at(4).substr(1, tempVector.at(4).length()));

    try{
        TimeCode* tc = new TimeCode();
        istringstream str(timeCode);
        str >> tc;

        rt->setTimeCode(tc);
        rt->setAuthor(author);
        rt->setMovieTitle(movieTitle);
        rt->setRating(rating);

    }catch(const invalid_argument& e){
        cerr << "Exception: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    return str;
}

inline bool Rating::operator==(Rating& rt){
    return this->getTimeCode()->operator==(*rt.getTimeCode());
}

inline bool Rating::operator!=(Rating& rt){
    return !(this->operator==(rt));
}

inline bool Rating::operator>(Rating& rt){
    return this->getTimeCode()->operator>(*rt.getTimeCode());
}

inline bool Rating::operator<(Rating& rt){
    return this->getTimeCode()->operator<(*rt.getTimeCode());
}

inline bool Rating::operator>=(Rating& rt){
    return !(this->operator<(rt));
}

inline bool Rating::operator<=(Rating& rt){
    return !(this->operator>(rt));
}

#endif