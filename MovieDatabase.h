#ifndef MOVIEDATABASE_MOVIEDATABASE_H
#define MOVIEDATABASE_MOVIEDATABASE_H
#include "Movie.h"
#include "Ratings.h"
#include <vector>
#include <sstream>
#include <algorithm>

class MovieDatabase {

private:
    vector<Movie*> movies;

    static void merge(Movie* arr[], int left, int middle, int right);

    static void mergeSort(Movie* arr[], int left, int right);
public:
    MovieDatabase();

    ~MovieDatabase();

    void rateMovies(Ratings* ratings);

    inline void setMovies(vector<Movie*> newMovies){
        movies = newMovies;
    }

    inline void sort(){
        //convert to array
        Movie* arr[getMovies().size()];

        for(int i = 0; i < movies.size(); i++){
            arr[i] = movies.at(i);
        }

        //sort the array
        mergeSort(arr, 0 , movies.size() - 1);

        //convert back to vector
        for(int i = 0; i < movies.size(); i++){
            movies.at(i) = arr[i];
        }
    }

    inline vector<Movie*> takeGenre(string genre){
        vector<Movie*> result;

        for(Movie* mov : movies){
            /*
            if(find(mov->getGenre().begin(), mov->getGenre().end(), genre) != mov->getGenre().end()) {
                result.push_back(mov);
            }
             */

            //it could be done by using code above
            //but did not work out for me by some reason :(
            for(string s : mov->getGenre()){
                if(!s.compare(genre)){
                    result.push_back(mov);
                }
            }
        }

        return result;
    }

    inline void add(Movie* movie){
        this->movies.push_back(movie);
    }

    inline vector<Movie*> getMovies() const{
        return this->movies;
    }
};

inline ostream& operator<<(ostream& str, const MovieDatabase* db){
    for(Movie* mov : db->getMovies()){
        str << "\"" << mov->getTitle() << "\"," << mov->getYear() << ",\"" << mov->getMPAArating() << "\",\"";

        for(string s : mov->getGenre()){
            str << s;

            //slashes between genres
            if(s.compare(mov->getGenre().at(mov->getGenre().size() - 1))){
                str << "/";
            }
        }
        str.precision(3);
        str << "\"," << mov->getLength() << "," << mov->getMeanRating() << "," << mov->getRatings() << "\n";
    }

    return str;
}

inline istream& operator>>(istream& str, MovieDatabase* db){
    string line; //represents single line in a file

    while (getline(str, line)) {
        //convert string to istream
        istringstream is(line);

        //create a movie
        Movie* mov = new Movie();
        is >> mov;

        //add it to the database
        db->add(mov);
    }
}

#endif
