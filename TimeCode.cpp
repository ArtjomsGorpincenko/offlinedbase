#include "TimeCode.h"

TimeCode::TimeCode() {

}

TimeCode::TimeCode(int newDay, int newMonth, int newYear, int newHour, int newMinute, int newSecond) {
    if(newDay < 1 || newDay > 31){
        throw invalid_argument("The passed day is less than 1 or more than 31.");
    }

    if(newMonth < 1 || newMonth > 12){
        throw invalid_argument("The passed month is less than 1 or more than 12.");
    }

    //get current year
    time_t theTime = time(NULL);
    auto aTime = localtime(&theTime);
    int currentYear = aTime->tm_year + 1900;

    //check if the passed year is less or equal to 0 or more than the current year
    if(newYear <= 0 || newYear > currentYear){
        throw invalid_argument("The passed year is less or equal to 0 or more than the current year.");
    }

    //February cases
    //leap year
    if(newMonth == 2 && newDay > 29 && newYear%4 == 0){
        throw invalid_argument("The passed day is more than 29 for February.");
    }

    //non leap year
    if(newMonth == 2 && newDay > 28 && newYear%4 != 0){
        throw invalid_argument("The passed day is more than 28 for February.");
    }

    if(newHour < 0 || newHour > 23){
        throw invalid_argument("The passed hour is less than 0 or more than 23.");
    }

    if(newMinute < 0 || newMinute > 59){
        throw invalid_argument("The passed minute is less than 0 or more than 59.");
    }

    if(newSecond < 0 || newSecond > 59){
        throw invalid_argument("The passed second is less than 0 or more than 59.");
    }

    this->day = newDay;
    this->month = newMonth;
    this->year = newYear;
    this->hour = newHour;
    this->minute = newMinute;
    this->second = newSecond;
}

TimeCode::~TimeCode(){
    //not sure if this line should be here, but just in case..
    delete this;
}

void TimeCode::setDay(int newDay){
    if(newDay < 1 || newDay > 31){
        throw invalid_argument("The passed day is less than 1 or more than 31.");
    }

    //February cases
    //leap year
    if(this->month == 2 && newDay > 29 && this->year%4 == 0){
        throw invalid_argument("The passed day is more than 29 for February.");
    }

    //non leap year
    if(this->month == 2 && newDay > 28 && this->year%4 != 0){
        throw invalid_argument("The passed day is more than 28 for February.");
    }

    this->day = newDay;
}

void TimeCode::setMonth(int newMonth){
    if(newMonth < 1 || newMonth > 12){
        throw invalid_argument("The passed month is less than 1 or more than 12.");
    }

    //February cases
    //leap year
    if(newMonth == 2 && this->day > 29 && this->year%4 == 0){
        throw invalid_argument("The passed day is more than 29 for February.");
    }

    //non leap year
    if(newMonth == 2 && this->day > 28 && this->year%4 != 0){
        throw invalid_argument("The passed day is more than 28 for February.");
    }

    this->month = newMonth;
}

void TimeCode::setYear(int newYear){
    //get current year
    time_t theTime = time(NULL);
    auto aTime = localtime(&theTime);
    int currentYear = aTime->tm_year + 1900;

    //check if the passed year is less or equal to 0 or more than the current year
    if(newYear <= 0 || newYear > currentYear){
        throw invalid_argument("The passed year is less or equal to 0 or more than the current year.");
    }

    //February cases
    //leap year
    if(this->month == 2 && this->day > 29 && newYear%4 == 0){
        throw invalid_argument("The passed day is more than 29 for February.");
    }

    //non leap year
    if(this->month == 2 && this->day > 28 && newYear%4 != 0){
        throw invalid_argument("The passed day is more than 28 for February.");
    }

    this->year = newYear;
}

void TimeCode::setHour(int newHour){
    if(newHour < 0 || newHour > 23){
        throw invalid_argument("The passed hour is less than 0 or more than 23.");
    }

    this->hour = newHour;
}

void TimeCode::setMinute(int newMinute){
    if(newMinute < 0 || newMinute > 59){
        throw invalid_argument("The passed minute is less than 0 or more than 59.");
    }

    this->minute = newMinute;
}

void TimeCode::setSecond(int newSecond){
    if(newSecond < 0 || newSecond > 59){
        throw invalid_argument("The passed second is less than 0 or more than 59.");
    }

    this->second = newSecond;
}
vector<string> TimeCode::split(string line, char delimiter) {
    vector<string> vector;

    //turn the string into a stream
    stringstream ss(line);

    string tempString;

    //split the line (stream) by the passed delimiter and add one by one to the vector
    while(getline(ss, tempString, delimiter)) {
        vector.push_back(tempString);
    }

    return vector;
}
