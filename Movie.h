#ifndef MOVIEDATABASE_MOVIE_H
#define MOVIEDATABASE_MOVIE_H

#include "TimeCode.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

class MovieDatabase;

namespace MPAArating {
    enum MPAArating {
        PG_13, PG, APPROVED, R, NOT_RATED, G, UNRATED, PASSED, N_A, TV_14, M, X
    };

    inline ostream& operator<<(ostream& str, const MPAArating rating){
        switch (rating){
            case(PG_13):
                return str << "PG-13";
            case(PG):
                return str << "PG";
            case(APPROVED):
                return str << "APPROVED";
            case(R):
                return str << "R";
            case(NOT_RATED):
                return str << "NOT RATED";
            case(G):
                return str << "G";
            case(UNRATED):
                return str << "UNRATED";
            case(PASSED):
                return str << "PASSED";
            case(N_A):
                return str << "N/A";
            case(TV_14):
                return str << "TV-14";
            case(M):
                return str << "M";
            case(X):
                return str << "X";
        }
    }

    inline istream& operator>>(istream& str, MPAArating& rating){
        string tempString;
        getline(str,tempString);

        if(tempString == "PG-13"){
            rating = MPAArating::PG_13;
            return str;
        }

        if(tempString == "PG"){
            rating = MPAArating::PG;
            return str;
        }

        if(tempString == "APPROVED"){
            rating = MPAArating::APPROVED;
            return str;
        }

        if(tempString == "NOT RATED"){
            rating = MPAArating::NOT_RATED;
            return str;
        }

        if(tempString == "R"){
            rating = MPAArating::R;
            return str;
        }

        if(tempString == "G"){
            rating = MPAArating::G;
            return str;
        }

        if(tempString == "UNRATED"){
            rating = MPAArating::UNRATED;
            return str;
        }

        if(tempString == "PASSED"){
            rating = MPAArating::PASSED;
            return str;
        }

        if(tempString == "N/A"){
            rating = MPAArating::N_A;
            return str;
        }

        if(tempString == "TV-14"){
            rating = MPAArating::TV_14;
            return str;
        }

        if(tempString == "M"){
            rating = MPAArating::M;
            return str;
        }

        if(tempString == "X"){
            rating = MPAArating::X;
            return str;
        }

        throw invalid_argument("The passed MPAArating does not match any defined value.");
    }
}

class Movie {

private:
    string title;
    int year;
    MPAArating::MPAArating MPAArating;
    vector<string> genre;
    int length;
    double meanRating;
    int ratings;
public:

    Movie(string newTitle, int newYear, string newMPAArating, string newGenres, int newLength,
          double newMeanRating = 0.0, int newRatings = 0);

    Movie();

    ~Movie();

    static vector<string> split(string str, char delimiter);

    void addRating(int rating);

    bool operator==(Movie& mov);

    bool operator!=(Movie& mov);

    bool operator>(Movie& mov);

    bool operator<(Movie& mov);

    bool operator>=(Movie& mov);

    bool operator<=(Movie& mov);

    inline string getTitle() const{
        return this->title;
    }

    inline int getYear() const{
        return this->year;
    }

    inline MPAArating::MPAArating getMPAArating() const{
        return this->MPAArating;
    }

    inline vector<string> getGenre() const{
        return this->genre;
    }

    inline int getLength() const{
        return this->length;
    }

    inline double getMeanRating() const{
        return this->meanRating;
    }

    inline int getRatings() const{
        return this->ratings;
    }

    void setTitle(string newTitle);

    void setYear(int newYear);

    void setMPAArating(string newMPAArating);

    void setGenre(string newGenres);

    void setLength(int newLength);

    void setRatings(int newRatings);

    void setMeanRating(double newMeanRating);
};

class Rating;

inline ostream& operator<<(ostream& str, const Movie* mov){
    //split length into hours and minutes
    int hours = mov->getLength() / 60;
    int minutes = mov->getLength() % 60;

    str <<  "Title\t\t\t: " << mov->getTitle() <<
            "\nRelease year\t: " << mov->getYear() <<
            "\nMPAA rating\t\t: " << mov->getMPAArating() <<
            "\nGenre(s)\t\t: ";

    for(string s : mov->getGenre()){
        str << s;

        //slashes between genres
        if(s.compare(mov->getGenre().at(mov->getGenre().size() - 1))){
            str << "/";
        }
    }
    str.precision(3);
    str <<  "\nLength\t\t\t: " << hours << "h " << std::setw(2) << std::setfill('0') << minutes << "m" <<
            "\nRating\t\t\t: "  << mov->getMeanRating() <<
            "\n№ of ratings\t: " << mov->getRatings() << "\n\n";

    return str;
}

inline istream& operator>>(istream& str, Movie* mov){
    //fields that are needed for Movie
    string title, year, MPAArating, genres, length;

    string tempString;
    getline(str,tempString);
    vector<string> tempVector = Movie::split(tempString, '"');

    title = tempVector.at(1);
    year = tempVector.at(2);

    //get rid of commas
    vector<string> tempYear = Movie::split(year, ',');
    year = tempYear.at(1);

    MPAArating = tempVector.at(3);
    genres = tempVector.at(5);
    length = tempVector.at(6);

    //get rid of commas
    vector<string> lastValues = Movie::split(length, ',');
    length = lastValues.at(1);

    try{
        mov->setTitle(title);
        mov->setYear(stoi(year));
        mov->setMPAArating(MPAArating);
        mov->setGenre(genres);
        mov->setLength(stoi(length));
    }catch(const invalid_argument& e){
        cerr << "Exception: " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    return str;
}

inline bool Movie::operator==(Movie& mov){
    return this->getYear() == mov.getYear() && this->getTitle() == mov.getTitle();
}

inline bool Movie::operator!=(Movie& mov){
    return !(this->operator==(mov));
}

inline bool Movie::operator>(Movie& mov){
    if(year > mov.getYear()){
        return true;
    } else if (year == mov.getYear() && title > mov.getTitle()){
        return true;
    }

    return false;
}

inline bool Movie::operator<(Movie& mov){
    if(year < mov.getYear()){
        return true;
    }else if(year == mov.getYear() && title < mov.getTitle()){
        return true;
    }

    return false;
}

inline bool Movie::operator>=(Movie& mov){
    return !(this->operator<(mov));
}

inline bool Movie::operator<=(Movie& mov){
    return !(this->operator>(mov));
}

#endif
