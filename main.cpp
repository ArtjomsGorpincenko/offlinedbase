#include <iostream>
#include "Movie.h"
#include "MovieDatabase.h"
#include "Rating.h"
#include "Ratings.h"

using namespace std;

bool sortByYear(Movie* first, Movie* scnd){
    return first->operator<(*scnd);
}

bool sortByLength(Movie* first, Movie* scnd) {
    return first->getLength() > scnd->getLength();
}

bool sortByTitleLength(Movie* first, Movie* scnd){
    return first->getTitle().length() > scnd->getTitle().length();
}

bool sortByRating(Movie* first, Movie* scnd){
    return first->getMeanRating() > scnd->getMeanRating();
}


static void printTaskHeader(string task){
    cout << "-------------------------------------------------" << endl;
    cout << "-\t-\t-\t-\t-\t-" << task << "-\t-\t-\t-\t-\t-" << endl;
    cout << "-------------------------------------------------" << endl;
}

static void task1(MovieDatabase* db){
    printTaskHeader("TASK 1");

    //take movies into temp vector
    vector<Movie*> temp = db->getMovies();

    //sort temp vector
    sort(temp.begin(), temp.end(), sortByYear);

    //set movies back
    db->setMovies(temp);

    //print info about the database
    cout << db << endl;
}

static void task2(MovieDatabase* db, string genre, int index){
    printTaskHeader("TASK 2");

    //put movies with specific genre in temp vector
    vector<Movie*> temp = db->takeGenre(genre);

    //sort temp vector by length
    sort(temp.begin(), temp.end(), sortByLength);

    //print details of a movie on a specific index
    cout << temp.at(index - 1) << endl;

    //free memory
    temp.clear();
}

static void task3(MovieDatabase* db, string genre, int index){
    printTaskHeader("TASK 3");

    //put movies with specific genre in temp vector
    vector<Movie*> temp = db->takeGenre(genre);

    //sort temp vector by rating
    sort(temp.begin(), temp.end(), sortByRating);

    //print details of a movie on a specific index
    cout << temp.at(index - 1) << endl;

    //free memory
    temp.clear();
}

static void task4(MovieDatabase* db, int index){
    printTaskHeader("TASK 4");

    /**
    //take movies into temp vector
    vector<Movie*> temp = db->getMovies();

    //sort temp vector
    sort(temp.begin(), temp.end(), sortByRating);

    //print details of a movie on a specific index
    cout << temp.at(index - 1) << endl;

    //free memory
    temp.clear();
     **/

    //the code below works if we have to find the highest rating
    //the code above will work on any index that will be provided (of course if it's 1 <= index <= n - 1)
    Movie* highest = new Movie();
    for(Movie* mov : db->getMovies()){
        if(mov->getMeanRating() > highest->getMeanRating()){
            highest = mov;
        }
    }

    cout << highest;
}

static void task5(MovieDatabase* db, int index){
    printTaskHeader("TASK 5");

    /**
    //put movies in temp vector
    vector<Movie*> temp = db->getMovies();

    //sort temp vector by length of their titles
    sort(temp.begin(), temp.end(), sortByTitleLength);

    //print details of a movie on a specific index
    cout << temp.at(index - 1) << endl;

    //free memory
    temp.clear();
    **/

    //the code below works if we have to find the longest title
    //the code above will work on any index that will be provided (of course if it's 1 <= index <= n - 1)
    Movie* highest = new Movie();
    for(Movie* mov : db->getMovies()){
        if(mov->getTitle().length() > highest->getTitle().length()){
            highest = mov;
        }
    }

    cout << highest;
}

static void task6(Ratings* rt, int index){
    printTaskHeader("TASK 6");

    //since I use set for Ratings, I don't need to sort them here
    Rating* x = *std::next(rt->getRatings().begin(), index - 1);
    cout << x;
}

int main() {
    //todo
    //default constructors IN EVERY CLASS
    //ratings >> db

    MovieDatabase* db = new MovieDatabase();
    Ratings* ratings = new Ratings();

    //load data from file
    ifstream file;
    file.open("C:\\movies.txt");
    file >> db;
    file.close();

    ifstream file1;
    file1.open("C:\\ratings.txt");
    file1 >> ratings;
    file1.close();

    db->rateMovies(ratings);

    task1(db);
    task2(db, "Film-Noir", 3);
    task3(db, "Sci-Fi", 10);
    task4(db, 1);
    task5(db, 1);
    task6(ratings, 101);
    return 0;
}

