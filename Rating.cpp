#include "Rating.h"

Rating::Rating() {

}

Rating::~Rating() {
    //not sure if this line should be here, but just in case..
    delete this;
}

Rating::Rating(TimeCode *newTimeCode, string newAuthor, string newMovieTitle, int newRating) {

    if(newAuthor.empty()){
        throw invalid_argument("The passed Author field is NULL or an empty string.");
    }

    if(newMovieTitle.empty()){
        throw invalid_argument("The passed Movie title is NULL or an empty string.");
    }

    if(newRating <= 0 || newRating > 11){
        throw invalid_argument("The passed rating is not in range 1-11.");
    }

    this->timeCode = newTimeCode;
    this->author = newAuthor;
    this->movieTitle = newMovieTitle;
    this->rating = newRating;
}

void Rating::setTimeCode(TimeCode* newTimeCode){
    this->timeCode = newTimeCode;
}

void Rating::setAuthor(string newAuthor){
    if(newAuthor.empty()){
        throw invalid_argument("The passed Author field is NULL or an empty string.");
    }

    this->author = newAuthor;
}

void Rating::setMovieTitle(string newMovieTitle){
    if(newMovieTitle.empty()){
        throw invalid_argument("The passed Movie title is NULL or an empty string.");
    }

    this->movieTitle = newMovieTitle;
}

void Rating::setRating(int newRating){
    if(newRating <= 0 || newRating > 11){
        throw invalid_argument("The passed rating is not in range 1-11.");
    }

    this->rating = newRating;
}