#include "Ratings.h"


Ratings::Ratings() {

}

Ratings::~Ratings() {
    //iterate through the set, delete each Rating in turn
    for (Rating* rt : ratings) {
        delete (rt);
    }

    //after that, we can delete the set itself
    ratings.clear();
}