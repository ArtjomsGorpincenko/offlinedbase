#include "MovieDatabase.h"
#include <algorithm>
#include <cmath>

using namespace std;

MovieDatabase::MovieDatabase(){

}

MovieDatabase::~MovieDatabase() {
    //iterate through the vector, delete each Movie in turn
    for (vector<Movie*>::iterator mov = movies.begin(); mov != movies.end(); ++mov) {
        delete (*mov);
    }

    //after that, we can delete the vector itself
    movies.clear();
}

void MovieDatabase::rateMovies(Ratings* ratings){
    //sort movies in lexicographical order so then we can find needed movie to assign a rating in O(log(n)) time
    //yes, it could be done by std::sort but I wanted to "play" with sorting by myself
    //std::sort is used in all other situations though (e.g. main.cpp)
    this->sort();

    for(Rating* rating : ratings->getRatings()){
        string movieTitle = rating->getMovieTitle();

        //binary search below
        int left = 0;
        int right = movies.size() - 1;

        while(left <= right){
            double temp = (left + right) / 2;
            int middle = floor(temp);

            if(movies.at(middle)->getTitle() < movieTitle){
                left = middle + 1;
            }else if(movies.at(middle)->getTitle() > movieTitle){
                right = middle - 1;
            }else{
                movies.at(middle)->addRating(rating->getRating());
                break;
            }
        }
    }
}

// Merges two subarrays of arr[].
// First subarray is arr[l..m]
// Second subarray is arr[m+1..r]
void MovieDatabase::merge(Movie* arr[], int left, int middle, int right) {
    int i, j, k;
    int n1 = middle - left + 1;
    int n2 =  right - middle;

    /* create temp arrays */
    Movie* leftArray[n1];
    Movie* rightArray[n2];

    /* Copy data to temp arrays leftArray[] and rightArray[] */
    for (i = 0; i < n1; i++){
        leftArray[i] = arr[left + i];
    }

    for (j = 0; j < n2; j++){
        rightArray[j] = arr[middle + 1+ j];
    }


    /* Merge the temp arrays back into arr[left..right]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = left; // Initial index of merged subarray
    while (i < n1 && j < n2) {
        if(leftArray[i]->getTitle() < rightArray[j]->getTitle()){
            arr[k] = leftArray[i];
            i++;
        } else {
            arr[k] = rightArray[j];
            j++;
        }
        k++;
    }

    /* Copy the remaining elements of leftArray[], if there
       are any */
    while (i < n1) {
        arr[k] = leftArray[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of rightArray[], if there
       are any */
    while (j < n2) {
        arr[k] = rightArray[j];
        j++;
        k++;
    }
}

/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
void MovieDatabase::mergeSort(Movie* arr[], int left, int right) {
    if (left < right) {
        // Same as (left+right)/2, but avoids overflow for
        // large left and h
        int m = left+(right-left)/2;

        // Sort first and second halves
        mergeSort(arr, left, m);
        mergeSort(arr, m+1, right);

        merge(arr, left, m, right);
    }
}