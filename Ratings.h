#ifndef MOVIEDATABASE_RATINGS_H
#define MOVIEDATABASE_RATINGS_H


#include <set>
#include "Rating.h"

//comparator
struct comparator {
    bool operator() (Rating* left, Rating* right) const{
        return left->operator<(*right);
    }
};

class Ratings {

private:

    set<Rating*, comparator> ratings;

public:

    Ratings();

    ~Ratings();

    inline set<Rating*, comparator> getRatings() const{
        return ratings;
    }

    void add(Rating* rt){
        ratings.insert(rt);
    }
};

inline ostream& operator<<(ostream& str, const Ratings* ratings){
    for(Rating* r : ratings->getRatings()){
        str << r;
    }

    return str;
}

inline istream& operator>>(istream& str, Ratings* ratings){
    string line; //represents single line in a file

    while (getline(str, line)) {
        //convert string to istream
        istringstream is(line);

        //create a rating
        Rating* rt = new Rating();
        is >> rt;

        //add it to the set
        ratings->add(rt);
    }
}


#endif
